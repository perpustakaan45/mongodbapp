from mongoengine import Document, StringField, DynamicDocument

class books(Document):
    nama = StringField(required=True, max_length=70)
    pengarang = StringField(required=True, max_length=20)
    tahunterbit = StringField(required=True, max_length=20)
    genre = StringField(required=True, max_length=20)