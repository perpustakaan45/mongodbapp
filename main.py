from mongoengine import connect
from models import books
from bson.objectid import ObjectId
#connection = connect(db="rentalfilm", host="localhost", port=27017)
class BooksQuery:
    def __init__(self):
        self.connection = connect(host="mongodb://127.0.0.1:27017/perpustakaan")

    def showBooks(self):
        res = books.objects
        return res

    def showBookById(self,id):
        res = books.objects.get(id=ObjectId(id))
        return res

    def showBookByName(self,name):
        res = books.objects(nama=name).first()
        return res

    def insertBook(self,**kwargs):
        books(**kwargs).save()

    def updateBookById(self,**kwargs):
        doc = books.objects.get(id=ObjectId(kwargs['id']))
        for key,value in kwargs.items():
            if hasattr(doc,key):
                setattr(doc, key, value)
        doc.save()
    
    def deleteBookById(Self,**kwargs):
        doc = books.objects.get(id=ObjectId(kwargs['id']))
        doc.delete()



if __name__ == '__main__':
    perpustakaan = BooksQuery()
    a = perpustakaan.showBooks()
    print(len(a))
    b = perpustakaan.showBookById("612e23587e4528163c9596d0")
    print(b.nama,b.pengarang,b.tahunterbit,b.genre)
    #test insert
    book_data = {
        "nama":"Sangonomiya Chronicles",
        "pengarang":"Unknown",
        "tahunterbit":"2021",
        "genre":"Fiction"
    }
    perpustakaan.insertBook(**book_data)
    c = perpustakaan.showBookByName("Sangonomiya Chronicles")
    print(c.id)
    #test update
    book_data_2 = book_data
    book_data_2["pengarang"] = "Sangonomiya Kokomi"
    book_data_2["id"] = c.id
    perpustakaan.updateBookById(**book_data_2)
    d = perpustakaan.showBookByName("Sangonomiya Chronicles")
    print(d.id,d.pengarang)
    #test delete
    perpustakaan.deleteBookById(**book_data_2)
    e = perpustakaan.showBookByName("Sangonomiya Chronicles")
    print(e.id,e.pengarang)

    

    
    # for doc in customers.objects:
    #     print (doc.username, doc.fullname, doc.email)

    # for doc in customers.objects(username='Fernanda'):
    #     print (doc.username, doc.fullname, doc.email)